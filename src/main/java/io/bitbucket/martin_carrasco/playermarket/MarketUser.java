/*
 * Copyright (C) 2016 Martin Carrasco <martin-1998 at hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.bitbucket.martin_carrasco.playermarket;

import static io.bitbucket.martin_carrasco.playermarket.Market.deserializeNBT;
import static io.bitbucket.martin_carrasco.playermarket.Market.serializeNBT;
import io.bitbucket.martin_carrasco.playermarket.SQL.AuctionExpiredTable;
import io.bitbucket.martin_carrasco.playermarket.SQL.AuctionListTable;
import io.bitbucket.martin_carrasco.playermarket.SQL.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_9_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class MarketUser 
{
    private final UUID uuid;
    private final Player player;
    public MarketUser(UUID uuid)
    {
        this.uuid = uuid;
        this.player = PlayerMarket.getInstance().getServer().getPlayer(uuid);
    }
    public UUID getUUID(){return uuid;}
    public void msg(String message, ChatColor color)
    {
        player.sendMessage(color + message);
    }
    public Player getPlayer(){return player;}
    public double getFunds(){return PlayerMarket.econ.getBalance(player);}
    public void takeFunds(double amount){PlayerMarket.econ.withdrawPlayer(player, amount);}
    public void addFunds(double amount){PlayerMarket.econ.depositPlayer(player, amount);}
    /**
     * Add item to auction list
     * @param i - Item to add
     * @param price - Price of said item
     */
    public void addItemAucList(ItemStack i, double price)
    {
        if(!PlayerMarket.sqlEnabled){
            CustomConfig conf = new CustomConfig(PlayerMarket.getInstance(), "data.yml");
            UUID id = UUID.randomUUID();
            conf.set("AucList."+uuid.toString()+id.toString()+".nbt", serializeNBT(i));
            conf.set("AucList."+uuid.toString()+id.toString()+".price", price);
            conf.set("AucList."+uuid.toString()+id.toString()+".time", System.currentTimeMillis() + TimeUnit.HOURS.toMillis(PlayerMarket.aucTimeExpire));
            conf.save();
            return;
        }
        try(Connection con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement(AuctionListTable.addItem);) {
            ps.setString(1, serializeNBT(i));
            ps.setString(2, uuid.toString());
            ps.setDouble(3, price);
            ps.setLong(4, System.currentTimeMillis() + TimeUnit.HOURS.toMillis(PlayerMarket.aucTimeExpire));
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MarketUser.class.getName()).log(Level.SEVERE, "Couldn't add item to AUCLIST DB", ex);
        }
    }
    /**
     * Add item to expired list
     * @param i - Item to remove
     */
    public void addItemAucExpired(ItemStack i)
    {
        if(!PlayerMarket.sqlEnabled){
            CustomConfig conf = new CustomConfig(PlayerMarket.getInstance(), "data.yml");
            conf.set("AucExpired."+uuid.toString()+UUID.randomUUID().toString()+".nbt", serializeNBT(i));
            conf.save();
            return;
        }
        try(Connection con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement(AuctionExpiredTable.addItem);) {
            ps.setString(1, serializeNBT(i));
            ps.setString(2, uuid.toString());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MarketUser.class.getName()).log(Level.SEVERE, "Couldn't add item to AUCEXPIRED DB", ex);
        }
    }
    /**
     * Remove item from auction list
     * @param i - Item to remove
     * @param price - Price of the item
     */
    public void remItemAucList(ItemStack i, double price)
    {
        if(!PlayerMarket.sqlEnabled){
            CustomConfig conf = new CustomConfig(PlayerMarket.getInstance(), "data.yml");
            if(conf.getConfigurationSection("AucList") == null)
                return;
            if(!conf.getConfigurationSection("AucList").contains(uuid.toString()))
                return;
            for(String s : conf.getConfigurationSection("AucList."+uuid).getKeys(false))
            {
                if(serializeNBT(i).equalsIgnoreCase(conf.getString("AucList."+uuid.toString()+"."+s+".nbt"))
                    && price == conf.getDouble("AucList."+uuid.toString()+"."+s+".price")){
                    conf.set("AucList."+uuid.toString()+"."+s, null);
                    conf.save();
                    break;
                }
            }
            return;
        }
        try(Connection con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement(AuctionListTable.delItem);) {
            ps.setString(1, serializeNBT(i));
            ps.setString(2, uuid.toString());
            ps.setDouble(3, price);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MarketUser.class.getName()).log(Level.SEVERE, "Couldn't delete item to AUCLIST DB", ex);
        }
        new Market().removeItem(i, uuid, price);
    }
    /**
     * Remove item from expired list
     * @param i - Item to remove
     */
    public void remItemAucExpired(ItemStack i)
    {
        if(!PlayerMarket.sqlEnabled){
            CustomConfig conf = new CustomConfig(PlayerMarket.getInstance(), "data.yml");
            if(conf.getConfigurationSection("AucExpired") == null)
                return;
            if(!conf.getConfigurationSection("AucExpired").contains(uuid.toString()))
                return;
            for(String s : conf.getConfigurationSection("AucExpired."+uuid).getKeys(false))
            {
                if(serializeNBT(i).equalsIgnoreCase(conf.getString("AucExpired."+uuid.toString()+"."+s+".nbt"))){
                    conf.set("AucExpired."+uuid.toString()+"."+s, null);
                    conf.save();
                    break;
                }
            }
            return;
        }
        try(Connection con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement(AuctionExpiredTable.delItem);) {
            ps.setString(1, serializeNBT(i));
            ps.setString(2, uuid.toString());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MarketUser.class.getName()).log(Level.SEVERE, "Couldn't delete item to AUCEXPIRED DB", ex);
        }
    }
    /**
     * Get auction expired items
     * @return - Expired auction items 
     */
    public List<ItemStack> getAucExpired()
    {
        List<ItemStack> its = new ArrayList<>();
        if(!PlayerMarket.sqlEnabled){
            CustomConfig conf = new CustomConfig(PlayerMarket.getInstance(), "data.yml");
            if(conf.getConfigurationSection("AucExpired") == null)
                return its;
            if(!conf.getConfigurationSection("AucExpired").contains(uuid.toString()))
                return its;
            for(String s : conf.getConfigurationSection("AucExpired."+uuid).getKeys(false))
            {
                its.add(CraftItemStack.asBukkitCopy(deserializeNBT(conf.getString("AucExpired."+uuid.toString()+"."+s))));
            }
            return its;
        }
        try(Connection con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement(AuctionExpiredTable.getOwnerAll);) {
            ps.setString(1, uuid.toString());
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                    its.add(CraftItemStack.asBukkitCopy(deserializeNBT(rs.getString(1))));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MarketUser.class.getName()).log(Level.SEVERE, "Couldn't load AUCEXPIRED from DB", ex);
        }
        return its;
    }
    /**
     * Get auction list of items
     * @return - Player auctioned items 
     */
    public List<ItemStack> getAucList()
    {
        List<ItemStack> its = new ArrayList<>();
        if(!PlayerMarket.sqlEnabled){
            CustomConfig conf = new CustomConfig(PlayerMarket.getInstance(), "data.yml");
            if(conf.getConfigurationSection("AucList") == null)
                return its;
            if(!conf.getConfigurationSection("AucList").contains(uuid.toString()))
                return its;
            for(String s : conf.getConfigurationSection("AucList."+uuid).getKeys(false))
            {
                its.add(CraftItemStack.asBukkitCopy(deserializeNBT(conf.getString("AucList."+uuid.toString()+"."+s))));
            }
            return its;
        }
        try(Connection con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement(AuctionListTable.getOwnerAll);) {
            ps.setString(1, uuid.toString());
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                    ItemStack item = CraftItemStack.asBukkitCopy(deserializeNBT(rs.getString(1)));
                    List<String> lore = new ArrayList<>();
                    ItemMeta im = item.getItemMeta();
                    if(im.hasLore()){
                        lore = im.getLore();
                    }
                    lore.add(ChatColor.GOLD + "Price: " + ChatColor.GREEN + PlayerMarket.econ.currencyNamePlural() + rs.getInt(2));
                    lore.add(ChatColor.GOLD + "Time Remaining: " + ChatColor.GREEN
                            + PlayerMarket.formatTime(TimeUnit.MILLISECONDS.toHours(rs.getLong(3) - System.currentTimeMillis())));
                    im.setLore(lore);
                    item.setItemMeta(im);
                    its.add(item);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MarketUser.class.getName()).log(Level.SEVERE, "Couldn't load AUCLIST from DB", ex);
        }
        return its;
    }
}
