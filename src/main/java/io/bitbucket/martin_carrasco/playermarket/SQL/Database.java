/*
 * Copyright (C) 2016 Martin Carrasco <martin-1998 at hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.bitbucket.martin_carrasco.playermarket.SQL;

import io.bitbucket.martin_carrasco.playermarket.PlayerMarket;
import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.dbcp2.BasicDataSource;

public final class Database 
{
    private static final BasicDataSource dataSource = new BasicDataSource();
    
    public static void start()
    {
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/" + PlayerMarket.sqlDB);
        dataSource.setUsername(PlayerMarket.sqlUser);
        dataSource.setPassword(PlayerMarket.sqlPass);
    }
   
    private Database(){}
    
    public static Connection getConnection() throws SQLException{
        return dataSource.getConnection();
    }
}
