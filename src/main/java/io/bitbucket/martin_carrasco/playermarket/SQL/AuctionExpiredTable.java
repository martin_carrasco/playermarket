/*
 * Copyright (C) 2016 Martin Carrasco <martin-1998 at hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.bitbucket.martin_carrasco.playermarket.SQL;

public class AuctionExpiredTable 
{
    public static final String name = "aucexpired";
    
    public static final String createMySQLTable = "CREATE TABLE IF NOT EXISTS " + name + "("
            + "id INT(4) NOT NULL PRIMARY KEY AUTO_INCREMENT, "
            + "nbt VARCHAR(500), "
            + "owner VARCHAR(36));";
    public static final String addItem = "INSERT INTO "+name+"(nbt, owner) VALUES(?,?);";
    public static final String delItem = "DELETE FROM "+name+" WHERE nbt=? AND owner=? LIMIT 1";
    public static final String getOwnerAll = "SELECT nbt FROM "+name+" WHERE owner=?;";
    public static final String getAll = "SELECT nbt, owner FROM "+name+";";
}
