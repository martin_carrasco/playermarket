/*
 * Copyright (C) 2016 Martin Carrasco <martin-1998 at hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.bitbucket.martin_carrasco.playermarket.SQL;

public class AuctionListTable 
{
    public static final String name = "auclist";
    
    public static final String createMySQLTable = "CREATE TABLE IF NOT EXISTS " + name + "("
            + "id INT(4) NOT NULL AUTO_INCREMENT PRIMARY KEY, "
            + "nbt VARCHAR(500), "
            + "time BIGINT, "
            + "owner VARCHAR(36), "
            + "price INT(10));";
    public static final String getOwnerAll = "SELECT nbt, price, time FROM " + name + " WHERE owner=?;";
    public static final String delItem = "DELETE FROM " + name + " WHERE nbt=? AND owner=? AND price=? LIMIT 1;";
    public static final String addItem = "INSERT INTO "+name + "(nbt, owner, price, time) VALUES(?,?,?,?);";
    public static final String getAll = "SELECT nbt, owner, price, time FROM "+ name+";";
}
