/*
 * Copyright (C) 2016 Martin Carrasco <martin-1998 at hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.bitbucket.martin_carrasco.playermarket;

import java.util.ArrayList;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CMD_ah extends Command
{
    public CMD_ah()
    {
        super("playermarket", "Player market base", "Usage: /pm", new ArrayList<String>(){{this.add("pm");}});
    }
    @Override
    public boolean execute(CommandSender cs, String arg1, String[] args) 
    {
        if (cs instanceof Player)
        {
            Market ah = new Market();
            MarketUser mu = new MarketUser(((Player)cs).getUniqueId());
            switch (args.length) {
                case 0:
                    if(mu.getPlayer().hasPermission("playermarket.market")){
                        mu.getPlayer().openInventory(ah.getInventory());
                        return true;
                    }
                    mu.msg(Message.noPerm, ChatColor.RED);
                    return false;
                case 2:
                    if(args[0].equalsIgnoreCase("sell"))
                    {
                        if(mu.getPlayer().hasPermission("playermarket.market.sell"))
                        {
                            if(mu.getPlayer().getInventory().getItemInMainHand() != null)
                            {
                                if(!PlayerMarket.maxAucSize)
                                {
                                    int size = mu.getAucList().size();

                                    if(size == 36){
                                        mu.msg("You can't add more items", ChatColor.RED);
                                        return false;
                                    }
                                    else if(size > 26){
                                        if(!mu.getPlayer().hasPermission("playermarket.market.sell.27")){
                                            mu.msg("You can't add more items", ChatColor.RED);
                                            return false;
                                        }
                                    }
                                    else if(size > 17){
                                        if(!mu.getPlayer().hasPermission("playermarket.market.sell.18")){
                                            mu.msg("You can't add more items to auction", ChatColor.RED);
                                            return false;
                                        }
                                    }
                                    else if(size > 8){
                                        if(!mu.getPlayer().hasPermission("playermarket.market.sell.9")){
                                           mu.msg("You can't add more items to auction", ChatColor.RED);
                                            return false;
                                        }
                                    }
                                }
                                ItemStack i = mu.getPlayer().getInventory().getItemInMainHand();
                                if(i == null){
                                    mu.msg("You are not holding an item", ChatColor.RED);
                                    return false;
                                }
                                ah.addItem(i, Integer.parseInt(args[1]), mu);
                                mu.getPlayer().getInventory().setItemInMainHand(null);
                                mu.msg("Item put on auction house!", ChatColor.GREEN);
                                return true;
                            }
                            mu.msg("You are not holding an item", ChatColor.RED);
                            return false;
                        }
                        mu.msg(Message.noPerm, org.bukkit.ChatColor.RED);
                        return false;
                    }
                    break;
            }
            mu.msg(usageMessage, ChatColor.RED);
            return false;
        }
        return false;
    }
}
