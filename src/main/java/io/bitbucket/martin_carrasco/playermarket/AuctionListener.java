/*
 * Copyright (C) 2016 Martin Carrasco <martin-1998 at hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.bitbucket.martin_carrasco.playermarket;

import java.util.List;
import java.util.UUID;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class AuctionListener implements Listener
{
    @EventHandler
    public void onClick(InventoryClickEvent e)
    {
        if(e.getInventory().getTitle().toLowerCase().contains("player market") 
                && e.getInventory().getTitle().split(" ").length == 3)
        {
            if(e.getCurrentItem() != null)
            {
                try{
                int page = Integer.parseInt(e.getInventory().getTitle().split(" ")[2]);
                ItemStack item = e.getCurrentItem();
                e.setCancelled(true);
                if(item.hasItemMeta() && item.getItemMeta().hasDisplayName())
                {
                    switch(ChatColor.stripColor(item.getItemMeta().getDisplayName()))
                    {
                        case "My Market":
                            e.getWhoClicked().openInventory(makeMyItems(e.getWhoClicked().getUniqueId()));
                            return;
                        case "My Expired":
                            e.getWhoClicked().openInventory(makeExpiredItems(e.getWhoClicked().getUniqueId()));
                            return;
                        case "Previous Page":
                            if(page > 1)
                            {
                                Market au = new Market(page-1);
                                e.getWhoClicked().openInventory(au.getInventory());
                            }
                            return;
                        case "Next Page":
                            Market au = new Market(page+1);
                            e.getWhoClicked().openInventory(au.getInventory());
                            return;
                        default:
                           break;
                    }
                }
                 e.getWhoClicked().openInventory(PlayerMarket.makeConfirmInv(item, "Player Market Confirm Menu"));}
                catch(NullPointerException except){}
            }
        }
        else if(e.getInventory().getTitle().toLowerCase().equalsIgnoreCase("My Market"))
        {
            if(e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR)
            {
                try{
                MarketUser du = new MarketUser(e.getWhoClicked().getUniqueId());
                ItemStack item = e.getCurrentItem();
                double price = Market.getItemPrice(item);
                e.setCancelled(true);
                if(item.hasItemMeta() && item.getItemMeta().hasDisplayName()
                        && ChatColor.stripColor(item.getItemMeta().getDisplayName()).equalsIgnoreCase("back")){
                    du.getPlayer().openInventory(new Market().getInventory());
                    return;
                }
                if(du.getAucExpired().size() == 36){
                    du.msg("Expired tab is full, pickup some items", ChatColor.RED);
                    return;
                }
                
                ItemMeta it= item.getItemMeta();
                List<String> lore = it.getLore();
                lore.remove(lore.size()-1);
                lore.remove(lore.size()-1);
                it.setLore(lore);
                item.setItemMeta(it);
                
                du.remItemAucList(item, price);
                du.addItemAucExpired(item);
                new Market().removeItem(item, du.getUUID(), price);
                
                du.getPlayer().openInventory(makeMyItems(du.getUUID()));}
                catch(NullPointerException excep){}
            }
        }
        else if(e.getInventory().getTitle().toLowerCase().equalsIgnoreCase("My Expired"))
        {
            if(e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR)
            {
                try{
                MarketUser du = new MarketUser(e.getWhoClicked().getUniqueId());
                ItemStack item = e.getCurrentItem();
                e.setCancelled(true);
                if(item.hasItemMeta() && item.getItemMeta().hasDisplayName()
                        && ChatColor.stripColor(item.getItemMeta().getDisplayName()).equalsIgnoreCase("back")){
                    du.getPlayer().openInventory(new Market().getInventory());
                    return;
                }
                e.getWhoClicked().getInventory().addItem(item);
                du.remItemAucExpired(item);
                du.getPlayer().openInventory(makeExpiredItems(du.getUUID()));}
                catch(NullPointerException excep){}
            }
        }
        else if(e.getInventory().getTitle().equalsIgnoreCase("Player Market Confirm Menu"))
        {
            if(e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR)
            {
                MarketUser du = new MarketUser(e.getWhoClicked().getUniqueId());
                ItemStack item = e.getInventory().getItem(4).clone();
                ItemStack click = e.getCurrentItem().clone();
                
                double price = Market.getItemPrice(item);
                MarketUser owner = new MarketUser(Market.getItemUUID(item));
                
                ItemMeta it= item.getItemMeta();
                List<String> lore = it.getLore();
                lore.remove(lore.size()-1);
                lore.remove(lore.size()-1);
                lore.remove(lore.size()-1);
                it.setLore(lore);
                item.setItemMeta(it);
                
                e.getInventory().setItem(4, item);
                
                e.setCancelled(true);
                if(click.equals(item))
                    return;
                if(ChatColor.stripColor(click.getItemMeta().getDisplayName()).equalsIgnoreCase("confirm"))
                {
                   if(du.getFunds() >= price)
                   {
                       Market auc = new Market();
                       if(auc.hasItem(item, owner.getUUID(), price))
                       {
                            owner.remItemAucList(item, price);
                            auc.removeItem(item, owner.getUUID(), price);
                            du.takeFunds(price);
                            owner.addFunds(price);
                            owner.msg("You sold an item in AH for " + PlayerMarket.econ.currencyNamePlural()  +price, ChatColor.GREEN);
                            du.msg("You bought an item in AH for " + PlayerMarket.econ.currencyNamePlural()  +price, ChatColor.GREEN);
                            du.getPlayer().getInventory().addItem(item);
                            du.getPlayer().closeInventory();
                            return;
                       }
                       du.msg("Item not longer available", ChatColor.RED);
                       return;
                   }
                   du.msg("Not enough funds", ChatColor.RED);
                }
                else if(ChatColor.stripColor(click.getItemMeta().getDisplayName()).equalsIgnoreCase("cancel"))
                    du.getPlayer().openInventory(new Market().getInventory());
            }
        }
    }
    private Inventory makeMyItems(UUID uuid)
    {
        Inventory inv = PlayerMarket.getInstance().getServer().createInventory(null, 45, "My Market");
        MarketUser du = new MarketUser(uuid);
        ItemStack back = new ItemStack(Material.BOOK);
        ItemMeta bm = back.getItemMeta();
        bm.setDisplayName(ChatColor.RED + "Back");
        back.setItemMeta(bm);
        inv.setItem(40, back);
        List<ItemStack> its = du.getAucList();
        for(int x = 0;x < its.size();x++)
        {
            if(x == 36)
                break;
            inv.setItem(x, its.get(x));
        }
        return inv;
    }
    private Inventory makeExpiredItems(UUID uuid)
    {
        Inventory inv = PlayerMarket.getInstance().getServer().createInventory(null, 45, "My Expired");
        MarketUser du = new MarketUser(uuid);
        ItemStack back = new ItemStack(Material.BOOK);
        ItemMeta bm = back.getItemMeta();
        bm.setDisplayName(ChatColor.RED + "Back");
        back.setItemMeta(bm);
        inv.setItem(40, back);
        List<ItemStack> its = du.getAucExpired();
        for(int x = 0;x < its.size();x++)
        {
            if(x == 36)
                break;
            inv.setItem(x, its.get(x));
        }
        return inv;
    }
}
