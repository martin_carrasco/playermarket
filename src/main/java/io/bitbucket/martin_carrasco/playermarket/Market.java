/*
 * Copyright (C) 2016 Martin Carrasco <martin-1998 at hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.bitbucket.martin_carrasco.playermarket;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import io.bitbucket.martin_carrasco.playermarket.SQL.AuctionTable;
import io.bitbucket.martin_carrasco.playermarket.SQL.Database;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.minecraft.server.v1_9_R1.NBTCompressedStreamTools;
import net.minecraft.server.v1_9_R1.NBTTagCompound;
import org.apache.commons.codec.binary.Base64;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_9_R1.inventory.CraftItemStack;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Market 
{
    private ItemStack nextPage;
    private ItemStack prevPage;
    private ItemStack myList;
    private ItemStack expired;
    private int pageNum;
    private Inventory inv;
    public Market()
    {
        this(1);
    }
    public Market(int pageNum)
    {
        this.pageNum = pageNum;
        inv = PlayerMarket.getInstance().getServer().createInventory(null, 45, "Player Market " + pageNum);
        
        nextPage = new ItemStack(Material.BOOK);
        ItemMeta npi = nextPage.getItemMeta();
        npi.setDisplayName(ChatColor.GREEN + "Next Page");
        nextPage.setItemMeta(npi);
        
        prevPage = new ItemStack(Material.BOOK);
        ItemMeta ppi = prevPage.getItemMeta();
        ppi.setDisplayName(ChatColor.GREEN + "Previous Page");
        prevPage.setItemMeta(ppi);
        
        expired = new ItemStack(Material.BOOK);
        ItemMeta ei = expired.getItemMeta();
        ei.setDisplayName(ChatColor.RED + "My Expired");
        expired.setItemMeta(ei);
        
        myList = new ItemStack(Material.BOOK);
        ItemMeta mli = myList.getItemMeta();
        mli.setDisplayName(ChatColor.YELLOW + "My Market");
        myList.setItemMeta(mli);
        
        
        inv.setItem(44, nextPage);
        inv.setItem(43, expired);
        inv.setItem(36, prevPage);
        inv.setItem(37, myList);
        
        getItems();
    }
    public boolean hasItem(ItemStack item, UUID uuid, double price)
    {
        if(!PlayerMarket.sqlEnabled)
        {
            CustomConfig conf = new CustomConfig(PlayerMarket.getInstance(), "data.yml");
            if(conf.getConfigurationSection("MarketItems") == null)
                return false;
            for(String s : conf.getConfigurationSection("MarketItems").getKeys(false))
            {
                if(conf.getString("MarketItems."+s+".nbt").equalsIgnoreCase(serializeNBT(item))
                        && conf.getString("MarketItems."+s+".owner").equalsIgnoreCase(uuid.toString())
                        && conf.getDouble("MarketItems."+s+".price") == price)
                {
                    return true;
                }
            }
            return false;
        }
        try(Connection con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement(AuctionTable.getAll);) {
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                if(serializeNBT(item).equals(rs.getString(1))
                        && uuid.toString().equals(rs.getString(2))
                        && price == rs.getInt(3))
                {
                    return true;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Market.class.getName()).log(Level.SEVERE, "Couldn't retrieve data from DB", ex);
        }
        return false;
    }
    public Inventory getInventory(){return inv;}
    public int getPage(){return pageNum;}
    public void addItem(ItemStack i, double price, MarketUser owner)
    {
        if(!PlayerMarket.sqlEnabled)
        {
            CustomConfig conf = new CustomConfig(PlayerMarket.getInstance(), "data.yml");
            UUID uuid = UUID.randomUUID();
            conf.set("MarketItems."+uuid.toString()+".nbt", serializeNBT(i));
            conf.set("MarketItems."+uuid.toString()+".owner", owner.toString());
            conf.set("MarketItems."+uuid.toString()+".price", price);
            conf.save();
            return;
        }
        try(Connection con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement(AuctionTable.addItem);) {
            ps.setString(1, serializeNBT(i));
            ps.setString(2, owner.getUUID().toString());
            ps.setDouble(3, price);
            ps.executeUpdate();
            owner.addItemAucList(i, price);
        } catch (SQLException ex) {
            Logger.getLogger(Market.class.getName()).log(Level.SEVERE, "Couldn't save item to DB", ex);
        }
    }
    public void removeItem(ItemStack i, UUID uuid, double price)
    {
        if(!PlayerMarket.sqlEnabled){
            CustomConfig conf = new CustomConfig(PlayerMarket.getInstance(), "data.yml");
            if(conf.getConfigurationSection("MarketItems") == null)
                return;
            for(String s : conf.getConfigurationSection("MarketItems").getKeys(false))
            {
                if(serializeNBT(i).equalsIgnoreCase(conf.getString("MarketItems."+s+".nbt"))
                    && price == conf.getDouble("MarketItems."+s+".price")
                    && uuid.toString().equalsIgnoreCase(conf.getString("MarketItems."+s+".owner"))){
                    conf.set("MarketItems."+s, null);
                    conf.save();
                    break;
                }
            }
            return;
        }
        try(Connection con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement(AuctionTable.delItem);) {
            ps.setString(1, serializeNBT(i));
            ps.setString(2, uuid.toString());
            ps.setDouble(3, price);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Market.class.getName()).log(Level.SEVERE, "Couldn't delete item from DB", ex);
        }
    }
    private void getItems()
    {        
        if(!PlayerMarket.sqlEnabled){
            CustomConfig conf = new CustomConfig(PlayerMarket.getInstance(), "data.yml");
            if(conf.getConfigurationSection("MarketItems") == null)
                return;
            List<String> cs = new ArrayList<>();
            cs.addAll(conf.getConfigurationSection("MarketItems").getKeys(false));
            for(int x = 36*(pageNum-1);x < 36*pageNum;x++)
            {
                ItemStack item = CraftItemStack.asBukkitCopy(deserializeNBT(conf.getString("MarktetItems."+cs.get(x)+".nbt")));
                    ItemMeta im = item.getItemMeta();
                    List<String> lore = new ArrayList<>();
                    if(im.hasLore()){
                        lore = im.getLore();
                    }
                    lore.add(ChatColor.GOLD + "Price: " + ChatColor.GREEN 
                            + PlayerMarket.econ.currencyNamePlural() + String.valueOf(conf.getDouble("MarketItems."+cs.get(x)+".price")));
                    lore.add(ChatColor.GOLD + "Owner: " + ChatColor.GREEN
                            + PlayerMarket.getInstance().getServer().getPlayer(UUID.fromString(conf.getString("MarketItems."+cs.get(x)+".owner"))).getName());
                    lore.add(ChatColor.GOLD + "UUID: " + ChatColor.GREEN + conf.getString("MarketItems."+cs.get(x)+".owner"));
                    im.setLore(lore);
                    item.setItemMeta(im);
                    inv.setItem(x, item);
            }
            return;
        }
        int count = 0;
        try(Connection con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement(AuctionTable.getAll);) {
                ResultSet rs = ps.executeQuery();
                rs.absolute(36*(pageNum-1));
                while(rs.next())
                {
                    if(count == 36)
                        break;
                    ItemStack item = CraftItemStack.asBukkitCopy(deserializeNBT(rs.getString(1)));
                    ItemMeta im = item.getItemMeta();
                    List<String> lore = new ArrayList<>();
                    if(im.hasLore()){
                        lore = im.getLore();
                    }
                    lore.add(ChatColor.GOLD + "Price: " + ChatColor.GREEN 
                            + PlayerMarket.econ.currencyNamePlural() + String.valueOf(rs.getDouble(3)));
                    lore.add(ChatColor.GOLD + "Owner: " + ChatColor.GREEN
                            + PlayerMarket.getInstance().getServer().getPlayer(UUID.fromString(rs.getString(2))).getName());
                    lore.add(ChatColor.GOLD + "UUID: " + ChatColor.GREEN + rs.getString(2));
                    im.setLore(lore);
                    item.setItemMeta(im);
                    inv.setItem(count, item);
                    count++;
                }
        } catch (SQLException ex) {
            Logger.getLogger(Market.class.getName()).log(Level.SEVERE, "Couldn't get items from DB", ex);
        }
    }
    public static String getItemOwner(ItemStack i)
    {
        if(i.hasItemMeta() && i.getItemMeta().hasLore())
        {
            for(String s : i.getItemMeta().getLore())
            {
                if(ChatColor.stripColor(s).toLowerCase().contains("owner:"))
                {
                    return ChatColor.stripColor(s).split(":")[1].replace(" ", "");
                }
            }
        }
        return "";
    }
    public static UUID getItemUUID(ItemStack i)
    {
        if(i.hasItemMeta() && i.getItemMeta().hasLore())
        {
            for(String s : i.getItemMeta().getLore())
            {
                if(ChatColor.stripColor(s).toLowerCase().contains("uuid:"))   
                {
                    return UUID.fromString(ChatColor.stripColor(s).split(":")[1].replace(" ", ""));
                }
            }
        }
        return null;
    }
    public static double getItemPrice(ItemStack i)
    {
        if(i.hasItemMeta() && i.getItemMeta().hasLore())
        {
            for(String s : i.getItemMeta().getLore())
            {
                if(ChatColor.stripColor(s).toLowerCase().contains("price:"))   
                {
                    return Double.parseDouble(ChatColor.stripColor(s).split(":")[1].replace(PlayerMarket.econ.currencyNamePlural(), "").replace(" ", ""));
                }
            }
        }
        return -1;
    }
    public static net.minecraft.server.v1_9_R1.ItemStack deserializeNBT(String s)
    {
        if(s.equals("null"))
                return null;
        try {
            ByteArrayInputStream stream = new ByteArrayInputStream(Base64.decodeBase64(s));
            DataInputStream in = new DataInputStream(stream);
            NBTTagCompound tag = NBTCompressedStreamTools.a(in);
            return net.minecraft.server.v1_9_R1.ItemStack.createStack(tag);
        } catch (IOException ex) {
            Logger.getLogger(Market.class.getName()).log(Level.SEVERE, "Error copying NBT data", ex);
        }
        return null;
    }
    public static String serializeNBT(ItemStack i)
    {
        try {
            NBTTagCompound tag = new NBTTagCompound();
            CraftItemStack.asNMSCopy(i).save(tag);
            ByteArrayDataOutput data = ByteStreams.newDataOutput();
            NBTCompressedStreamTools.a(tag, data);
            return Base64.encodeBase64String(data.toByteArray());
        } catch (IOException ex) {
            Logger.getLogger(Market.class.getName()).log(Level.SEVERE, "Error compressing NBT tag", ex);
        }
        return "null";
    }
}
