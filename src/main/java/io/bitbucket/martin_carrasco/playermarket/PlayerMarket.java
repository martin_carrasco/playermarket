/*
 * Copyright (C) 2016 Martin Carrasco <martin-1998 at hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.bitbucket.martin_carrasco.playermarket;

import io.bitbucket.martin_carrasco.playermarket.SQL.AuctionExpiredTable;
import io.bitbucket.martin_carrasco.playermarket.SQL.AuctionListTable;
import io.bitbucket.martin_carrasco.playermarket.SQL.AuctionTable;
import io.bitbucket.martin_carrasco.playermarket.SQL.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_9_R1.CraftServer;
import org.bukkit.craftbukkit.v1_9_R1.inventory.CraftItemStack;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class PlayerMarket extends JavaPlugin
{
    public static boolean maxAucSize;
    public static long aucTimeExpire;
    public static String sqlDB;
    public static String sqlUser;
    public static String sqlPass;
    public static boolean sqlEnabled;
    
    private CustomConfig conf;
    public static Economy econ;
    private static PlayerMarket instance;
    @Override
    public void onEnable()
    {
        instance = this;
        this.saveDefaultConfig();
        econ = getServer().getServicesManager().getRegistration(Economy.class).getProvider();
        initConfig();
        ((CraftServer)this.getServer()).getCommandMap().register(this.getName(), new CMD_ah());
        if(sqlEnabled)
        {
            Database.start();
            initMySQL();
        }
        else
            conf = new CustomConfig(this, "data.yml");
        startTask();
    }
    private void initMySQL()
    {
        try(Connection connection = Database.getConnection())
        {
            connection.prepareStatement(AuctionExpiredTable.createMySQLTable).executeUpdate();
            connection.prepareStatement(AuctionTable.createMySQLTable).executeUpdate();
            connection.prepareStatement(AuctionListTable.createMySQLTable).executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PlayerMarket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void initConfig()
    {
        maxAucSize = this.getConfig().getBoolean("maxAucSize");
        aucTimeExpire = this.getConfig().getLong("aucTimeExpire");
        sqlDB = this.getConfig().getString("sqlDB");
        sqlUser = this.getConfig().getString("sqlUser");
        sqlPass = this.getConfig().getString("sqlPass");
        sqlEnabled = this.getConfig().getBoolean("sqlEnabled");
    }
    public static PlayerMarket getInstance()
    {
        return instance;
    }
    public static String formatTime(long time)
    {
        long hours = TimeUnit.MILLISECONDS.toHours(time);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(time - TimeUnit.HOURS.toMillis(hours));
        long seconds = TimeUnit.MILLISECONDS.toSeconds(TimeUnit.HOURS.toMillis(hours) - TimeUnit.MINUTES.toMillis(minutes));
        return String.format("%d hours %02d min %02d secs", 
                hours,
                minutes,
                seconds);
    }
   public static Inventory makeConfirmInv(ItemStack i, String menuName)
    {
        Inventory inv = PlayerMarket.getInstance().getServer().createInventory(null,9,menuName);
        
        ItemStack green = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)13);
        ItemMeta im = green.getItemMeta();
        im.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "CONFIRM");
        green.setItemMeta(im);
        
        ItemStack red = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)14);
        im = red.getItemMeta();
        im.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "CANCEL");
        red.setItemMeta(im);
        
        for(int x = 0; x < 9;x++)
        {
            if(x < 4){inv.setItem(x, green);}
            else if(x == 4){inv.setItem(x, i);}
            else{inv.setItem(x, red);}
        }
        return inv;
    }
   public void startTask()
   {
        this.getServer().getScheduler().runTaskTimer(this, new Runnable()
        {
            @Override
            public void run()
            {
                if(!sqlEnabled)
                {
                    CustomConfig conf = new CustomConfig(PlayerMarket.getInstance(), "data.yml");
                    if(conf.getConfigurationSection("MarketItems") == null)
                        return;
                    for(String s : conf.getConfigurationSection("MarketItems").getKeys(false))
                    {
                        if(conf.getLong("MarketItems."+s+".time") < System.currentTimeMillis()){
                            double price = conf.getDouble("MarketItems."+s+".price");
                            MarketUser mu = new MarketUser(UUID.fromString(conf.getString("MarketItems."+s+".owner")));
                            ItemStack item = CraftItemStack.asBukkitCopy(Market.deserializeNBT(conf.getString("MarketItems."+s+".nbt")));
                            mu.addItemAucExpired(item);
                            mu.remItemAucList(item, price);
                            new Market().removeItem(item, mu.getUUID(), price);
                        }
                    }
                    return;
                }
                try(Connection connection = Database.getConnection();
                    PreparedStatement ps = connection.prepareStatement(AuctionListTable.getAll);) 
                {
                    ResultSet rs = ps.executeQuery();
                    while(rs.next())
                    {
                        if(rs.getLong(4) < System.currentTimeMillis())
                        {
                            ItemStack item = CraftItemStack.asBukkitCopy(Market.deserializeNBT(rs.getString(1)));
                            MarketUser du = new MarketUser(UUID.fromString(rs.getString(2)));
                            du.remItemAucList(item, rs.getInt(3));
                            du.addItemAucExpired(item);
                            new Market().removeItem(item, du.getUUID(), rs.getInt(3));
                        }
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(PlayerMarket.class.getName()).log(Level.SEVERE, "Couldn't update item AUCLIST", ex);
                }
            }
        }, 20*60, 20*60*10);
   }
}
